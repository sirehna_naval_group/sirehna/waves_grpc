"""Bretschneider spectrum."""

import math
from airy import Airy
import waves


class Bretschneider:
    """Linear irregular waves in infinite depth.

    Using a Bretschneider spectrum and a single direction. No stretching.
    """

    def __init__(self):
        """Constructor."""
        self.name = "Bretschneider spectrum"
        self.hs_squared = None
        self.omega0 = None

    def customize(self, param):
        """Initialize the wave model with YAML parameters.

        Parameters
        ----------
        parameters : string
            YAML string containing the parameters of this model. The YAML
            should have the following form:
                waves propagating to: 90
                Hs: 5
                Tp: 15
            `waves propagating to`: direction, in degrees,  the waves are
                                    propagating to. 0 for waves coming from the
                                    South and propagating to the North, 90 deg
                                    for waves coming from the East and
                                    propagating to the West.
            `Hs`: significant wave height in meters, used in the JONSWAP
                  spectrum.
            `Tp`: peak wave period, in seconds, used in the JONSWAP spectrum.
            `omega`: discretization of the angular frequency. In rad/s.
            `psi`: discretization of the directions. In degrees.


        Returns
        -------
        Nothing

        """
        self.hs_squared = param['Hs']*param['Hs']
        self.omega0 = 2*math.pi/param['Tp']

    def spectrum(self, omega):
        """Bretschneider spectrum."""
        alpha = math.pow(self.omega0/omega, 4)
        return 0.3125 * (alpha/omega) * self.hs_squared * math.exp(-1.25*alpha)


if __name__ == '__main__':
    waves.serve(Airy(Bretschneider()))
