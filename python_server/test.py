"""Unit tests of the wave modules."""

import json
from airy import Airy
from bretschneider import Bretschneider
from jonswap import Jonswap


def test_bretschneider():
    """Check the Bretschneider spectrum implementation. Cf. xdyn's tests."""
    model = Airy(Bretschneider())
    hs1_tp5 = {'Hs': 1, 'Tp': 5, 'omega': [1, 2, 3], 'waves propagating to': 0,
               'psi': [1]}
    hs2_tp5 = {'Hs': 2, 'Tp': 5, 'omega': [1, 2, 3], 'waves propagating to': 0,
               'psi': [1]}
    hs2_tp10 = {'Hs': 2, 'Tp': 10, 'omega': [1, 2, 3], 'waves propagating to': 0,
                'psi': [1]}
    model.set_parameters(json.dumps(hs1_tp5))
    eps = 1E-15
    assert abs(model.spectrum_model.spectrum(1) - 0.034510725921325) < eps
    model.set_parameters(json.dumps(hs2_tp5))
    assert abs(model.spectrum_model.spectrum(1) - 0.138042903685298) < eps
    assert abs(model.spectrum_model.spectrum(10) - 0.000031161194389) < eps
    model.set_parameters(json.dumps(hs2_tp10))
    assert abs(model.spectrum_model.spectrum(0.5) - 0.276085807370596) < eps


def test_jonswap():
    """Check the JONSWAP spectrum implementation. Cf. xdyn's tests."""
    model = Airy(Jonswap())
    p = {'Hs': 1, 'Tp': 10, 'gamma': 3.3, 'omega': [1, 2, 3], 'waves propagating to': 0}
    model.set_parameters(json.dumps(p))
    eps = 1E-15
    assert abs(model.spectrum_model.spectrum(0.4) - 0.001548977510098) < eps
    assert abs(model.spectrum_model.spectrum(0.5) - 0.046145520023689) < eps
    assert abs(model.spectrum_model.spectrum(0.6) - 0.241661465488933) < eps
    assert abs(model.spectrum_model.spectrum(0.7) - 0.144435240716521) < eps
    assert abs(model.spectrum_model.spectrum(0.8) - 0.061449169843241) < eps
