"""JONSWAP spectrum."""

import math
from airy import Airy
import waves


class Jonswap:
    """Linear irregular waves in infinite depth.

    Using JONSWAP spectrum and single propagation direction. No stretching.
    """

    def __init__(self):
        """Constructor."""
        self.sigma_a = 0.07
        self.sigma_b = 0.09
        self.gamma = None
        self.hs_square = None
        self.omega0 = None
        self.coeff = None
        self.name = "JONSWAP spectrum"

    def customize(self, param):
        """Initialize the wave model with YAML parameters.

        Parameters
        ----------
        parameters : string
            YAML string containing the parameters of this model. The YAML
            should have the following form:
                waves propagating to: 90
                Hs: 5
                Tp: 15
                gamma: 1.2
                omega: [1,2,3]
            `waves propagating to`: direction, in degrees,  the waves are
                                    propagating to. 0 for waves coming from the
                                    South and propagating to the North, 90 deg
                                    for waves coming from the East and
                                    propagating to the West.
            `Hs`: significant wave height in meters, used in the JONSWAP
                  spectrum.
            `Tp`: peak wave period, in seconds, used in the JONSWAP spectrum.
            `gamma`: JONSWAP shape parameter
            `omega`: discretization of the angular frequency. In rad/s.
            `psis`: discretization of the incidence. In rad.


        Returns
        -------
        Nothing

        """
        self.gamma = param['gamma']
        self.hs_square = param['Hs']*param['Hs']
        self.omega0 = 2*math.pi/param['Tp']
        self.coeff = 1-0.287*math.log(param['gamma'])

    def spectrum(self, omega):
        r"""Joint North Sea Project spectrum.

        ```math
        S(\omega)=(1-0.287 \log(\gamma))\frac{5}{16}\frac{\alpha}{\omega}H_S^2
        e^{-1.25\left(\frac{\omega_0}{\omega}\right)^4}\gamma^r
        ```

        with

        ```math
        r=e^{-0.5\left(\frac{\omega-\omega_0}{\sigma\omega_0}\right)^2}
        ```

        and

        ```math
        \sigma=\left\{\begin{array}{l}0.07,\omega\leq\omega_0\\0.09,
        \omega>\omega_0\end{array}\right.
        ```
        """
        sigma = self.sigma_a if omega <= self.omega0 else self.sigma_b
        ratio = self.omega0/omega
        alpha = ratio*ratio*ratio*ratio
        awm_5 = self.coeff*5.0/16.0*alpha/omega*self.hs_square
        bwm_4 = 1.25*alpha
        kappa = (omega-self.omega0)/(sigma*self.omega0)
        return awm_5*math.exp(-bwm_4)*math.pow(self.gamma,
                                               math.exp(-0.5*kappa*kappa))


if __name__ == '__main__':
    waves.serve(Airy(Jonswap()))
