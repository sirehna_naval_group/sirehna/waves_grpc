FROM python:3.7-slim-stretch as base
ADD requirements.txt .
RUN python -m pip install -r requirements.txt && \
    mkdir -p /usr/python && \
    chmod a+rwx /usr/python
ENV PYTHONPATH /usr/python
ADD wave_types.proto .
ADD wave_grpc.proto .
RUN python -m grpc_tools.protoc -I . --python_out=/usr/python --grpc_python_out=/usr/python wave_types.proto
RUN python -m grpc_tools.protoc -I . --python_out=/usr/python --grpc_python_out=/usr/python wave_grpc.proto
WORKDIR /usr/python/

FROM base
RUN pip3 install \
    nose==1.3.7\
    rednose==1.3.0\
    pylint==2.4.3\
    pylint-protobuf==0.11
ADD pylint.rc /
ADD airy.py jonswap.py bretschneider.py test.py waves.py /usr/python/
RUN pylint --rcfile=/pylint.rc --load-plugins=pylint_protobuf airy.py jonswap.py bretschneider.py waves.py test.py
RUN nosetests -x -v -d --nocapture --nologcapture --rednose

FROM base
ARG model
ADD airy.py jonswap.py bretschneider.py waves.py /usr/python/
RUN ln -s $model model.py
ENTRYPOINT ["python", "model.py"]
